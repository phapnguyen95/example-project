import {
  BadRequestException,
  Body,
  Controller,
  Post,
  Res,
} from '@nestjs/common';
import { AppService } from './app.service';
import { JwtService } from '@nestjs/jwt';
import { Response, Request } from 'express';
import { Get } from '@nestjs/common';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private jwtService: JwtService,
  ) {}

  // @Post('register')
  // async register(
  //   @Body('name') name: string,
  //   @Body('email') email: string,
  //   @Body('password') password: string,
  // ) {
  //   return this.appService.create({
  //     name,
  //     email,
  //     password,
  //   });
  // }
  @Post('login')
  async login(
    @Body('email') email: string,
    @Body('password') password: string,
    @Res({ passthrough: true }) response: Response,
  ) {
    const user = await this.appService.findOne({ email });
    if (!user) {
      throw new BadRequestException('error 401');
    }
    if (password != user.password) {
      throw new BadRequestException('error 401');
    }
    const jwt = await this.jwtService.sign({ id: user.id });
    response.cookie('jwt', jwt, { httpOnly: true });
    return jwt;
  }
  @Get('user')
  async user(@Res() request: Request) {
    const cookie = request.cookies['jwt'];
    const data = await this.jwtService.verify(cookie);
    return data;
  }
}
