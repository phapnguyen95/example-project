import { Injectable, Controller } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './user.entity';
import { Repository } from 'typeorm';
@Injectable()
export class AppService {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
  ) {}
  // async create(data: any) {
  //   return this.userRepository.save(data);
  // }
  async findOne(conditon: any): Promise<User> {
    return this.userRepository.findOne(conditon);
  }
}
